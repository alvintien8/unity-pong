#pragma strict

var ballScript : ball;
var powerupType : int = 0;

function AssignPowerup() {
	if (powerupType == 0) {
		ballScript.VelocitySlowdownPowerup();
	} else if (powerupType == 1) {
		ballScript.VelocitySpeedupPowerup();
	} else {
		Debug.Log("invalid powerup type");
	}
}

function OnTriggerEnter(collision : Collider) {
	if(collision.transform.name == "Ball") {
		AssignPowerup();
		Destroy(gameObject);
	}
}