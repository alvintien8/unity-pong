#pragma strict

private var ray : Ray;
private var rayCastHit : RaycastHit;

function Update () {
	var name;
	var i = 0;
	if (Input.touchCount > 0) {
		
		for(i = 0; i < Input.touchCount; i++) {
			ray = GetComponent.<Camera>().main.ScreenPointToRay(Input.GetTouch(i).position);
			if(Physics.Raycast(ray, rayCastHit)) {
				name = rayCastHit.collider.gameObject.name;
				if(name == "P2TapDetectionBox" || name == "PaddleLeft2" || name == "PaddleRight2"
				|| name == "PaddleTopLeft2" || name == "PaddleTopRight2" || name == "PaddleMiddle2")
					transform.position.x = rayCastHit.point.x;
			} 
			
			//make sure the paddle does not go out of bounds.
			if(transform.position.x > 2.3) {
				transform.position.x = 2.3;
			} else if (transform.position.x < -2.3){
				transform.position.x = -2.3;
			}
		}
	}
}