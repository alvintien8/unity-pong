#pragma strict

var player1lives : int = 5;
var player2lives : int = 5;

//display text
var p1LivesText : TextMesh;
var p2LivesText : TextMesh;
var p1Text : TextMesh;
var p2Text : TextMesh;

var returnBtn : GameObject;

private var posX : float = -1.727147;
private var posY : float = 1.323647;
private var numPowerups : int = 2;

//powerup objects
var powerupSlowdown : GameObject;
var powerupSpeedup : GameObject;

function Awake() {
	p1LivesText.text = "Lives: " + player1lives.ToString();
	p2LivesText.text = "Lives: " + player2lives.ToString();
	//spawnObjects();
}

function Update() {
	if (Input.GetKeyDown(KeyCode.Escape)) { Application.LoadLevel("menu"); }
}

function updateP1Lives() {
	CancelInvoke ("spawnPowerups");
	player1lives--;
	p1LivesText.text = "Lives: " + player1lives.ToString();

}

function updateP2Lives() {
	CancelInvoke ("spawnPowerups");
	player2lives--;
	p2LivesText.text = "Lives: " + player2lives.ToString();
}

function p1Loses() {
	p2Text.text = "Winner!";
	Instantiate(returnBtn,Vector3(posX,posY,0), Quaternion.identity);
}


function p2Loses() {
	p1Text.text = "Winner!";
	Instantiate(returnBtn,Vector3(posX,posY,0), Quaternion.identity);
}

function spawnObjects() {
	InvokeRepeating("spawnPowerups",20,20);
}

function spawnPowerups() {
	var posX : float = Random.Range(-3,4); //from -3 to 3
	var posY : float = Random.Range(-2,5); //from -1 to 1
	
	//chooses which object to spawn
	var object : int = Random.Range(0,numPowerups); 
	
	if (object == 0) {
		Instantiate(powerupSlowdown,Vector3(posX,posY,0), Quaternion.identity);
	} else if (object == 1) {
		Instantiate(powerupSpeedup,Vector3(posX,posY,0), Quaternion.identity);
	}
}