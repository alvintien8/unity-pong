#pragma strict

#pragma strict
var mainGameScript : BattleMode;
var particles_splash : GameObject;
private var seconds : int = 3;
var textMeshP1 : TextMesh;
var textMeshP2 : TextMesh;

function Awake() {
	moveBall();
}

function Update() {
	//game over state
	if(transform.position.y < -4) {
		//Application.LoadLevel("menu");
		mainGameScript.updateP1Lives();
		
		if (mainGameScript.player1lives <= 0) {
	    	mainGameScript.p1Loses();
	    	Destroy(gameObject);
		} else {
			resetBall();
		}
	} else if (transform.position.y > 6) {
		mainGameScript.updateP2Lives();
		
		if (mainGameScript.player2lives <= 0) {
	    	mainGameScript.p2Loses();
	    	Destroy(gameObject);
		} else {
			resetBall();
		}
	}
}

function resetBall() {
	CancelInvoke ("IncreaseBallVelocity");
	transform.position.x = 0.07325554;
	transform.position.y = 0.8527902;
	GetComponent.<Rigidbody>().velocity *= 0;
	countdownInit();
}

function countdownInit() {
 	//textMesh = GameObject.Find ("Timer").GetComponent(TextMesh);
    textMeshP1.text = seconds.ToString();
    textMeshP2.text = seconds.ToString();
    InvokeRepeating ("Countdown", 1.0, 1.0);
}

function Countdown () {
    if (--seconds == 0) {
    	CancelInvoke ("Countdown");
    	seconds = 3; //reset the timer
    	textMeshP1.text = "";
    	textMeshP2.text = "";
		resumePlay();
    } else {
    	textMeshP1.text = seconds.ToString();
    	textMeshP2.text = seconds.ToString();
    }
}

function moveBall() {
	GetComponent.<Rigidbody>().AddForce(4,4,0,ForceMode.Impulse);
	InvokeRepeating("IncreaseBallVelocity",2,2);
}

function resumePlay() {
	moveBall();
	//resume powerups
	//mainGameScript.spawnObjects();
}

function IncreaseBallVelocity() {
	GetComponent.<Rigidbody>().velocity *= 1.05;
	//Debug.Log("Ball Velocity: " + rigidbody.velocity);
}

function OnCollisionEnter(collision : Collision) {
	Instantiate(particles_splash,transform.position, transform.rotation);
	GetComponent.<AudioSource>().Play();
}

//used for powerups
function VelocitySlowdownPowerup() {
	GetComponent.<Rigidbody>().velocity *= 0.5;
}

function VelocitySpeedupPowerup() {
	GetComponent.<Rigidbody>().velocity *= 1.5;
}