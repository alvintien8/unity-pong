#pragma strict

var ballScript : ball;
var mainGameScript : MainGame;
var powerupType : int = 0;

function AssignPowerup() {
	if (powerupType == 0) {
		ballScript.VelocitySlowdownPowerup();
	} else if (powerupType == 1) {
		ballScript.VelocitySpeedupPowerup();
	} else if (powerupType == 2) {
		mainGameScript.AddPowerupScore();
	} else if (powerupType == 3) {
		mainGameScript.AddPowerupLives();
	} else {
		Debug.Log("invalid powerup type");
	}

}

function OnTriggerEnter(collision : Collider) {
	if(collision.transform.name == "Ball") {
		AssignPowerup();
		GetComponent.<AudioSource>().Play();
		yield WaitForSeconds(0.18);
		Destroy(gameObject);
	}
}