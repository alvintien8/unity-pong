#pragma strict

var Score3DText : TextMesh;
var Lives3DText : TextMesh;

private var score : int = 0;
var lives : int = 3;
private var gameOverDelay : int = 5;

//powerup objects
var numPowerups : int = 4;
var powerupSlowdown : GameObject;
var powerupSpeedup : GameObject;
var powerupExtralife : GameObject;
var powerupAddscore : GameObject;

function Awake() {
	IncrementScore();
	spawnObjects();
}

function Update() {
	if (Input.GetKeyDown(KeyCode.Escape)) { Application.LoadLevel("menu"); }
}

function IncrementScore() {
	InvokeRepeating("UpdateScore", 0.05, 0.05);
}

function UpdateScore() {
	score += 1;
	Score3DText.text = "Score: " + score.ToString();
}

function UpdateLives() {
	lives--;
	Lives3DText.text = "Lives: " + lives.ToString();
	//pause any functions that spawn powerups or increment score
	CancelInvoke ("UpdateScore");
	CancelInvoke ("spawnPowerups");
}

function AddScoreForBonusArea() {
	score += 50;
}

function GameOver() {	
	if(score > PlayerPrefs.GetInt("highScore")) {
		PlayerPrefs.SetInt("highScore", score);
	}
	Application.LoadLevel("menu");
}

//for powerups
function spawnObjects() {
	InvokeRepeating("spawnPowerups",20,20);
}

function spawnPowerups() {
	var posX : int = Random.Range(-3,4); //from -3 to 3
	var posY : int = Random.Range(-1,2); //from -1 to 1
	
	//chooses which object to spawn
	var object : int = Random.Range(0,numPowerups); 
	
	if (object == 0) {
		Instantiate(powerupSlowdown,Vector3(posX,posY,0), Quaternion.identity);
	} else if (object == 1) {
		Instantiate(powerupSpeedup,Vector3(posX,posY,0), Quaternion.identity);
	} else if (object == 2) {
		Instantiate(powerupAddscore,Vector3(posX,posY,0), Quaternion.identity);
	} else if (object == 3) {
		Instantiate(powerupExtralife,Vector3(posX,posY,0), Quaternion.identity);
	}
}

function AddPowerupScore() {
	score += 100;
}

function AddPowerupLives() {
	lives++;
	Lives3DText.text = "Lives: " + lives.ToString();
}