#pragma strict
var mainGameScript : MainGame;
var particles_splash : GameObject;
private var seconds : int = 3;
var textMesh : TextMesh;

function Awake() {
	moveBall();
}

function Update() {
	//game over state
	if(transform.position.y < -4) {
		//Application.LoadLevel("menu");
		mainGameScript.UpdateLives();
		
		if (mainGameScript.lives <= 0) {
	    	mainGameScript.GameOver();
		} else {
			resetBall();
		}
	}
}

function resetBall() {
	CancelInvoke ("IncreaseBallVelocity");
	transform.position.x = 0.01463699;
	transform.position.y = 2.329191;
	GetComponent.<Rigidbody>().velocity *= 0;
	countdownInit();
}

function countdownInit() {
 	//textMesh = GameObject.Find ("Timer").GetComponent(TextMesh);
    textMesh.text = seconds.ToString();
    InvokeRepeating ("Countdown", 1.0, 1.0);
}

function Countdown () {
    if (--seconds == 0) {
    	CancelInvoke ("Countdown");
    	seconds = 3; //reset the timer
    	textMesh.text = "";
		resumePlay();
    } else {
    	textMesh.text = seconds.ToString();
    }
}

function moveBall() {
	GetComponent.<Rigidbody>().AddForce(4,4,0,ForceMode.Impulse);
	InvokeRepeating("IncreaseBallVelocity",2,2);
}

function resumePlay() {
	moveBall();
	//resume the score modifier and powerups
	mainGameScript.IncrementScore();
	mainGameScript.spawnObjects();
}

function IncreaseBallVelocity() {
	GetComponent.<Rigidbody>().velocity *= 1.05;
	Debug.Log("Ball Velocity: " + GetComponent.<Rigidbody>().velocity);
}

function OnCollisionEnter(collision : Collision) {
	Instantiate(particles_splash,transform.position, transform.rotation);
	GetComponent.<AudioSource>().Play();
}

//used for powerups
function VelocitySlowdownPowerup() {
	GetComponent.<Rigidbody>().velocity *= 0.5;
}

function VelocitySpeedupPowerup() {
	GetComponent.<Rigidbody>().velocity *= 1.5;
}