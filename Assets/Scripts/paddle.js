#pragma strict

private var ray : Ray;
private var rayCastHit : RaycastHit;

function Update () {
	if (Input.GetMouseButton(0)) {
		ray = GetComponent.<Camera>().main.ScreenPointToRay(Input.mousePosition);
		
		if(Physics.Raycast(ray, rayCastHit)) {
			transform.position.x = rayCastHit.point.x;
		}
		
		//make sure the paddle does not go out of bounds.
		if(transform.position.x > 2.3) {
			transform.position.x = 2.3;
		} else if (transform.position.x < -2.3){
			transform.position.x = -2.3;
		}
	}
}