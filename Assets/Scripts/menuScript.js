#pragma strict

private var ray : Ray;
private var rayCastHit : RaycastHit;

var highScore3DText : TextMesh;
function Awake() {
	highScore3DText.text = "High Score: " + PlayerPrefs.GetInt("highScore").ToString();
}

function Update () {
	if (Input.GetKeyDown(KeyCode.Escape)) { Application.Quit(); }
	if (Input.GetMouseButton(0)) {
		ray = GetComponent.<Camera>().main.ScreenPointToRay(Input.mousePosition);
		
		if(Physics.Raycast(ray, rayCastHit)) {
			if(rayCastHit.transform.name == 'PlayButton') {
				Application.LoadLevel("MainGame");
			} else if(rayCastHit.transform.name == 'BattleModeButton') {
				Application.LoadLevel("BattleMode");
			}
		}
	
	}
}