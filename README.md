# README #

Alvin's Pong game made with Unity Game Engine.

### What is this repository for? ###

Single player breakout game plus a PvP mode. 

### How do I get set up? ###

1. Set up the Unity game engine
2. Check out this repository and add it as a new unity project.

### Who do I talk to? ###

Contact Alvin prior to making any contributions.